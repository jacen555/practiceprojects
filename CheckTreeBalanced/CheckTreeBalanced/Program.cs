﻿using System;
using System.Collections.Generic;

namespace CheckTreeBalanced
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        /// <summary>
        /// Returns whether or not each subtree has the same number of children
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        public bool IsBalanced(Node root)
        {
            return NumChildren(root) != -1;
        }

        /// <summary>
        /// Note: returns -1 if unbalanced. Otherwise returns the number of child nodes
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        public int NumChildren(Node branch)
        {
            int children = 0;

            if(branch.Children == null || branch.Children.Count == 0)
            {
                return 0;
            }
            else
            {
                int leafCount = 0;
                foreach(var child in branch.Children)
                {
                    int temp = NumChildren(child);
                    if (temp == -1)
                        return -1;
                    else if (leafCount == 0) // haven't set the children count for the first child yet
                        leafCount = temp;
                    else if (leafCount != temp)
                        return -1;
                    else
                        children += temp;
                }
            }

            return children;
        }
    }


    public class Node
    {
        public int Value
        {
            get;
            set;
        }

        public List<Node> Children
        {
            get;
            set;
        }

        public Node()
        {
        }
    }
}
