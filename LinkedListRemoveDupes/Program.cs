﻿using System;
using System.Collections.Generic;

namespace LinkedListRemoveDupes
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<int> orig = new LinkedList<int>();
            LinkedList<int> val;
            orig.AddLast(1);
            val = RemoveDupes(orig);
            orig.AddLast(1);
            val = RemoveDupes(orig);
            orig.AddLast(2);
            orig.AddLast(1);
            val = RemoveDupes(orig);
            orig.AddLast(2);
            orig.AddLast(3);
            orig.AddLast(4);
            val = RemoveDupes(orig);
        }

        /// <summary>
        /// Returns a new array with duplicates removed
        /// </summary>
        /// <param name="orig"></param>
        /// <returns></returns>
        public static LinkedList<int> RemoveDupes(LinkedList<int> orig)
        {
            LinkedList<int> retVal = new LinkedList<int>();
            LinkedListNode<int> last = retVal.Last;

            if(orig != null)
            {
                foreach (int val in orig)
                {
                    bool exists = false;
                    foreach (int toCheck in retVal)
                    {
                        if (toCheck == val)
                        {
                            exists = true;
                            break;
                        }
                    }
                    if(!exists)
                    {
                        // for some reason .NET doesn't want us to do this, but I suspect it's what it does behind the scenes
                        //last.Next = new LinkedListNode<int>(val);
                        //last = last.Next;
                        retVal.AddLast(val);
                    }
                }
            }

            return retVal;
        }
    }
}
