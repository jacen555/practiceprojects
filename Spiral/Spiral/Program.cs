﻿using System;

namespace Spiral
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] board = Spiral(3);
            // 1 2 3
            // 8 9 4
            // 7 6 5

            board = Spiral(4);
            //01 02 03 04
            //12 13 14 05
            //11 16 15 06
            //10 09 08 07

            board = Spiral(8);
            //01 02 03 04 05 06 07 08
            //28 29 30 31 32 33 34 09
            //27 48 49 50 51 52 35 10
            //26 47 60 61 62 53 36 11
            //25 46 59 64 63 54 37 12
            //24 45 58 57 56 55 38 13
            //23 44 43 42 41 40 39 14
            //22 21 20 19 18 17 16 15
        }

        /// <summary>
        /// Creates a "spiral" n x n board
        /// </summary>
        /// <param name="n">the dimensions of the board (square)</param>
        /// <returns></returns>
        public static int[,] Spiral(int n)
        {
            if (n == 0)
                return new int[0, 0];

            int[,] board = new int[n,n];
            int x = 0, y = 0, curVal = 1;
            Direction curDirection = Direction.Right;

            while (true)
            {
                if (board[x, y] == 0) // if it's not zero, then we have reached the end of the spiral
                {
                    board[x, y] = curVal;
                    curVal++;

                    SetNextStep(ref x, ref y, ref curDirection, board);
                }
                else
                {
                    break;
                }
            }

            return board;
        }

        /// <summary>
        /// Calculates the next step and sets the x,y, and direction
        /// </summary>
        /// <param name="x">the current x position. Is updated to the next</param>
        /// <param name="y">the current y position. Is updated to the next</param>
        /// <param name="curDirection">the current direction. Is updated to the next</param>
        /// <param name="max">the maximum value the x or y can be</param>
        /// <param name="min">the minimum value the x or y can be</param>
        public static void SetNextStep(ref int x, ref int y, ref Direction curDirection, int[,] board)
        {
            int max = board.GetLength(0);
            switch (curDirection)
            {
                case Direction.Right:
                    if (x + 1 >= max || board [x+1,y] != 0) // if the next step hits an edge (is beyond the end of the array or hits a number)
                    {
                        curDirection = Direction.Down;
                        y++;
                    }
                    else
                    {
                        x++;
                    }
                    break;
                case Direction.Down:
                    if (y + 1 >= max || board[x, y + 1] != 0)
                    {
                        curDirection = Direction.Left;
                        x--;
                    }
                    else
                    {
                        y++;
                    }
                    break;
                case Direction.Left:
                    if (x - 1 < 0 || board[x - 1, y] != 0)
                    {
                        curDirection = Direction.Up;
                        y--;
                    }
                    else
                    {
                        x--;
                    }
                    break;
                case Direction.Up:
                    if (y - 1 < 0 || board[x, y - 1] != 0)
                    {
                        curDirection = Direction.Right;
                        x++;
                    }
                    else
                    {
                        y--;
                    }
                    break;
            }
        }

        public enum Direction // we move clockwise
        {
            Right = 0,
            Down,
            Left,
            Up,
        }
    }
}
