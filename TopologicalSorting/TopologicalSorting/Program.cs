﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TopologicalSorting
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        /// <summary>
        /// Returns a "topological sort" ie: a list of nodes in order of what nodes need to occur before it
        /// </summary>
        /// <param name="unsortedList"></param>
        /// <returns></returns>
        static List<GraphItems> TopologicalSorting(List<GraphItems> unsortedList)
        {
            List<GraphItems> sortedList = new List<GraphItems>();
            List<GraphItems> tmpLst = unsortedList.Select(t => t).ToList();

            foreach (var node in unsortedList) // add all the nodes that don't need anything ahead of them first
            {
                if (node.Parent == null)
                {
                    tmpLst.Remove(node);
                    sortedList.Add(node);
                }
            }

            while (tmpLst.Count != 0)
            {
                List<GraphItems> toAdd = new List<GraphItems>(); // .NET doesn't like us making changes to a list while iterating over it. Create a temp list to keep track of what to move from one list to the other
                foreach (var node in tmpLst)
                {
                    if (sortedList.Contains(node.Parent))
                    {
                        toAdd.Add(node);
                    }
                }
                foreach (var node in toAdd)
                {
                    tmpLst.Remove(node);
                    sortedList.Add(node);
                }
            }

            return sortedList;
        }

        /// <summary>
        /// Returns a "topological sort" ie: a list of nodes in order of what nodes need to occur before it
        /// </summary>
        /// <param name="unsortedList"></param>
        /// <returns></returns>
        static List<GraphItems2> TopologicalSorting2(GraphItems2 startingNode)
        {
            List<GraphItems2> sortedList = new List<GraphItems2>();

            var curNode = startingNode;

            while (curNode.Parent != null) // navigate to the top of the list
            {
                curNode = curNode.Parent;
            }

            Queue<GraphItems2> tieredqueue = new Queue<GraphItems2>(); // depth-first addition
            tieredqueue.Enqueue(curNode);
            while(tieredqueue.Count > 0)
            {
                curNode = tieredqueue.Dequeue();
                sortedList.Add(curNode);
                foreach (var node in curNode.Children)
                    tieredqueue.Enqueue(node);
            }

            return sortedList;
        }
    }

    public class GraphItems
    {
        public GraphItems Parent
        {
            get;
            set;
        }

        public object Data
        {
            get;
            set;
        }
    }

    public class GraphItems2
    {
        public GraphItems2 Parent
        {
            get;
            set;
        }

        public List<GraphItems2> Children
        {
            get;
            set;
        }

        public object Data
        {
            get;
            set;
        }
    }
}
