﻿using System;
using System.Collections.Generic;

namespace StringContainsUniqueChar
{
    class Program
    {
        static void Main(string[] args)
        {
            bool val = ContainsUnique("");
            val = ContainsUnique("a");
            val = ContainsUnique("abc");
            val = ContainsUnique("aabc");
            val = ContainsUnique("abcc");
        }

        static bool ContainsUnique(string strToCheck)
        {   // optional: do it using no additional data structures (other than return value)
            // If there are enough characters to check, then
            //   compare each character against the characters that come before it 
            //   to determine if each is unique
            bool isInique = true;
            if (strToCheck != null && strToCheck.Length > 1)
            {
                for (int i = 1; i < strToCheck.Length; i++)
                {
                    for (int j = 0; j < i; j++)
                    {
                        if(strToCheck[i] == strToCheck[j])
                        {
                            isInique = false;
                            break;
                        }
                    }
                    if (!isInique) // break out early. Don't need to keep checking
                        break;
                }
            }
            return isInique;
        }
    }
}
