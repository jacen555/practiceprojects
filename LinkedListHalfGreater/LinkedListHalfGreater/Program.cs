﻿using System;
using System.Collections.Generic;

namespace LinkedListHalfGreater
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<int> orig = new LinkedList<int>();
        }

        /// <summary>
        /// splits the list so that all elements before "val" are less and all after are greater than
        /// </summary>
        /// <param name="list"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public static LinkedList<int> HalfSort(LinkedList<int> list, int val)
        {
            LinkedList<int> retVal = new LinkedList<int>();
            if (list != null && list.Contains(val))
            {
                retVal.AddFirst(val);
                LinkedListNode<int> splitPoint = retVal.Last;

                foreach (int node in list)
                {
                    if (node > val)
                    {
                        retVal.AddLast(node);
                    }
                    else if (node < val)
                    {
                        retVal.AddFirst(node);
                    }
                    else
                    {
                        //splitPoint.Next = new LinkedListNode<int>(node);
                        //splitPoint = splitPoint.Next;
                        retVal.AddAfter(splitPoint, node);
                        splitPoint = splitPoint.Next;
                    }
                }
            }

            return retVal;
        }
    }
}
