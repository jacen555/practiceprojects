﻿using System;
using System.Collections.Generic;

namespace BreadthFirstSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Dictionary<char, int> map = new Dictionary<char, int>();
            if(map.ContainsKey)
            foreach(var pair in map)
            {
            }

            List<Tuple<int, int>> tupLst = new List<Tuple<int, int>>();

            foreach(Tuple<int, int> tup in tupLst)
                tup.Item1
        }

        static Node BreadthSearch(Node root, int val )
        {
            Queue<Node> queue = new Queue<Node>();

            if (root.Value == val)
                return root;
            queue.Enqueue(root);

            while(queue.Count != 0)
            {
                Node n = queue.Dequeue();
                if(n.Children.Count > 0)
                {
                    foreach (var child in n.Children)
                    {
                        if (!child.Visited)
                        {
                            if (child.Value == val) // we search each layer before checking each of the children
                                return child;
                            queue.Enqueue(child);
                        }
                    }
                }
            }


            return null;
        }
    }

    public class Node
    {
        public bool Visited
        {
            get;
            set;
        }

        public int Value
        {
            get;
            set;
        }

        public List<Node> Children
        {
            get;
            set;
        }

        public Node()
        {
            Visited = false;
        }
    }
}
