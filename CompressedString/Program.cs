﻿using System;
using System.Linq;
using System.Text;

namespace CompressedString
{
    class Program
    {
        static void Main(string[] args)
        {
            string val = GetCompressedString("aabbcc");
            val = GetCompressedString("aa");
            val = GetCompressedString("aaabbcc");
            val = GetCompressedString("abacbc");
            val = GetCompressedString("aaabbbcccd");
            val = GetCompressedString("aabcccccaaa");
        }

        /// <summary>
        /// Returns a "compressed" version of the string. If can't be compressed, then return the existing string
        ///   (ie: aabbcc=> a2b2c2)
        /// </summary>
        /// <returns></returns>
        static string GetCompressedString(string toCompress)
        {
            if (toCompress == null || toCompress.Length <= 2) // if it's two characters or less, then we can't compress.
                return toCompress;

            StringBuilder builder = new StringBuilder(); // use a builder because it's faster than using a string
            bool canBeCompressed = false;
            char prevChar = toCompress.First();
            ushort count = 1;

            for (int i = 1; i < toCompress.Length; i++)
            {
                if (prevChar == toCompress[i])
                {
                    count++;
                }
                else
                {
                    if (count == 1)
                        builder.Append(prevChar);
                    else
                        builder.Append(prevChar + count.ToString());

                    if (count > 2)
                        canBeCompressed = true;

                    count = 1;
                    prevChar = toCompress[i];
                }
            }
            if (count == 1)
                builder.Append(prevChar);
            else
                builder.Append(prevChar + count.ToString());


            if (!canBeCompressed)
                return toCompress;
            else
                return builder.ToString();
        }
    }
}
