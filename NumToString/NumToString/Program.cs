﻿using System;
using System.Collections.Generic;

namespace NumToString
{
    class Program
    {
        private static Dictionary<int, char> map = new Dictionary<int, char>()
        {
            { 1,'a' },
            { 2,'b' },
            { 3,'c' },
            { 4,'d' },
            { 5,'e' },
            { 6,'f' },
            { 7,'g' },
            { 8,'h' },
            { 9,'i' },
            { 10,'j' },
            { 11,'k' },
            { 12,'l' },
        };

        static void Main(string[] args)
        {
            int tmp = 0;
            tmp = GetNumWays("1");
            tmp = GetNumWays("11");
            tmp = GetNumWays("111");
            tmp = GetNumWays("1111");
        }

        /*
           -Assume there's a map of numbers to characters like the following:
            - 1=>A, 2=>B,3=>C ... 26=>Z
            -output the number of different ways an input string of numbers can be decoded. IE: "11" can be "AA" or "K" so the output would be 2.
            -input: string, output: int (count)
         */
        public static int GetNumWays(string number)
        {
            return NumAsString(number).Count;
        }

        public static int CountDecode(string number)
        {
            if (number == null || number.Length == 0)
            {
                return 1;
            }
            else if(number.Length == 1)
            {
                if (!map.ContainsKey(number[0]))
                    return 0;
                else
                    return 1;
            }
            else
            {
                int count = 0;
                if (number[number.Length - 1] == '0')
                    count = CountDecode(number.Substring(number.Length - 1)); // can only be 10.

                count += CountDecode(number.Substring(number.Length - 2));

                return count;
            }
        }

        public static List<string> NumAsString(string number)
        {
            if (number == null || number.Length == 0)
                return new List<string>();
            else if (number.Length == 1)
                return new List<string>() { GetMap(Int32.Parse(number)).ToString() };
            else if (number.Length == 2)
            {
                List<string> tmp = new List<string>();
                tmp.Add(GetMap(Int32.Parse(number)).ToString());
                tmp.Add(GetMap(Int32.Parse(number[0].ToString())).ToString() + GetMap(Int32.Parse(number[1].ToString())));
                return tmp;
            }
            else
            {
                List<string> tmp = new List<string>();
                int half = number.Length / 2;
                tmp.AddRange(NumAsString(number.Substring(0, half)));
                tmp.AddRange(NumAsString(number.Substring(half, number.Length - half)));

                return tmp;
            }
        }

        /// <summary>
        /// returns the mapped version of the number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static char GetMap(int number)
        {
            return map[number];
        }
    }
}
