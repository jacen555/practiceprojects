﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DetermineStringsArePermutations
{
    class Program
    {
        static void Main(string[] args)
        {
            bool val = ArePermutations("a", "a");
            val = ArePermutations("a", "abc");
            val = ArePermutations("aaa", "abc");
            val = ArePermutations("abc", "abc");
            val = ArePermutations("abc", "cba");
            val = ArePermutations("aabc", "abc");
            val = ArePermutations("aabc", "aabc");
            val = ArePermutations("aabc", "abca");
            val = ArePermutations("aabc", "abcc");
        }

        /// <summary>
        /// Returns whether or not the two strings are permutations of eachother
        ///   (whether all the characters are in both strings in the same quantities)
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        static bool ArePermutations(string first, string second)
        {
            // to check if two strings are permutations of eachother, we check to see if their
            //   characters appear the same amount of times in both strings.
            //   Note: this isn't the most efficient way to do it for two strings, but
            //   it is the most resusable. For two strings, it's better to generate one 
            //   dictionary as we go and then break out early. For more than two, however
            //   this functionality still works where-as generating it on-the-fly would
            //   be much more expensive
            Dictionary<char, int> charCountFirst = GetCharCount(first);
            Dictionary<char, int> charCountSecond = GetCharCount(second);
            
            return Compare(charCountFirst, charCountSecond);
        }

        static Dictionary<char, int> GetCharCount(string str)
        {
            Dictionary<char, int> charToCount = new Dictionary<char, int>();

            if(!String.IsNullOrEmpty(str))
            {
                foreach(char c in str)
                {
                    if (!charToCount.ContainsKey(c))
                        charToCount.Add(c, 1);
                    else
                        charToCount[c]++;
                }
            }

            return charToCount;
        }

        static bool Compare(Dictionary<char, int> first, Dictionary<char, int> second)
        {
            bool match = true;
            // if they have the same number of elements and each element of first is in the second
            if (first.Count == second.Count)
            {
                foreach (KeyValuePair<char, int> pair in first)
                {
                    if (second[pair.Key] != pair.Value)
                    {
                        match = false;
                        break;
                    }
                }
            }
            else
            {
                match = false;
            }
            return match;
        }

        /// <summary>
        /// An example of reuse with a variable number of strings to compare
        /// </summary>
        /// <param name="strToCheck"></param>
        /// <returns></returns>
        static bool ArePermutations(List<string> strToCheck)
        {
            // the char count
            List<Dictionary<char, int>> countForEach = new List<Dictionary<char, int>>();
            bool arePerms = true;

            countForEach.Add(GetCharCount(strToCheck.First()));
            for(int i = 1; i < strToCheck.Count; i++)
            {
                Dictionary<char, int> toCompare = GetCharCount(strToCheck[i]);
                foreach (var dict in countForEach)
                {
                    if(!Compare(toCompare, dict))
                    {
                        arePerms = false;
                        break;
                    }
                }
                if (!arePerms)
                    break;
                else
                    countForEach.Add(toCompare);
            }

            return arePerms;
        }
    }
}
