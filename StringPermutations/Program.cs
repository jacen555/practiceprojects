﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StringPermutations
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> perm1 = Permutations("a");
            List<string> perm2 = Permutations("ab");
            List<string> perm3 = Permutations("abc");
            List<string> perm4 = Permutations("abcd");
        }

        /// <summary>
        /// Returns all the permutations of the sent in string str
        ///   Note: recursive
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static List<string> Permutations(string str)
        {
            if (str == null)
            {
                return new List<string>();
            }
            else
            {
                if (str.Length == 1)
                {
                    List<string> retVal = new List<string>
                    {
                        str
                    };
                    return retVal;
                }
                else
                {
                    // C"a"=>   {"a"}
                    // C"ab"=>  {"ab,"ba"}
                    // C"abc"=> {"cab","acb","abc"} join {"cba","bca","bac"}
                    string splitA = str.Substring(0, str.Length - 1);
                    string splitB = new string(str.Last(), 1);

                    List<string> prevPerm = Permutations(splitA); // Permutations(n-1)
                    List<string> retVal = new List<string>();
                    foreach (string perm in prevPerm)
                    {
                        retVal.AddRange(Interleave(perm, splitB));
                    }

                    return retVal;
                }
            }
        }

        /// <summary>
        /// returns an array of strings with the char C interleaved in between a different character each time
        /// </summary>
        /// <param name="str"></param>
        /// <param name="chr">a single character string</param>
        /// <returns></returns>
        public static List<string> Interleave(string str, string chr)
        {
            List<string> retVal = new List<string>();

            if (str == null || str.Length == 0)
            {
                retVal.Add(chr);
            }
            else
            {
                for (int i = 0; i <= str.Length; i++)
                {
                    retVal.Add(str.Insert(i, chr));
                }
            }

            return retVal;
        }
    }
}
