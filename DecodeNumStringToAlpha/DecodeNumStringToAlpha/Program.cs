﻿using System;

namespace DecodeNumStringToAlpha
{
    class Program
    {
        /// <summary>
        /// Let 1 represent ‘A’, 2 represents ‘B’, etc. 
        /// Given a digit sequence, count the number of possible decodings of the given digit sequence.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            int tmp = 0;
            tmp = GetNumWays("1");
            tmp = GetNumWays("11");
            tmp = GetNumWays("111");
            tmp = GetNumWays("1111");
        }

        static int Decode(string str)
        {
            // we're supposing that int.TryParse was called before. Don't want to call it here since it'll increasingly add time
            if (String.IsNullOrEmpty(str)) // if there are no characters in the string, then there are no decodings
                return 0;
            else if (str[0] == '0') // if the first digit is zero, then there are no possible decodings. Return zero
                return 0;
            else if (str.Length == 1) // if there is only one character in the string, then there is only one decoding
                return 1;
            else if (str[str.Length - 1] == '0') // the length is at least two and the last digit is a zero. Remove the last two (since 0 can only be 10 or 20) and recurse
            {
                return 1 + Decode(str.Substring(0, str.Length - 2));
            }
            else if (str[str.Length - 2] == '1' || str[str.Length - 2] == '2' && str[str.Length - 1] < '7') // If the last two digits can be decoded as a group or separately, then remove the last two and recurse
            {
                return 2 + Decode(str.Substring(0, str.Length - 2));
            }
            else // there's only one decoding for the last digit. Remove it and recurse
            {
                return 1 + Decode(str.Substring(0, str.Length - 1));
            }
        }
    }
}
