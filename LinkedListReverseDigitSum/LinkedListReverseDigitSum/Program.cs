﻿using System;
using System.Collections.Generic;

namespace LinkedListReverseDigitSum
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        /// <summary>
        /// Sums two linked lists that have their digits reversed
        /// </summary>
        /// <param name="list1"></param>
        /// <param name="list2"></param>
        /// <returns></returns>
        public static int SumReverseDigits(LinkedList<int> list1, LinkedList<int> list2)
        {
            return ReverseDigits(list1) + ReverseDigits(list2);
        }

        /// <summary>
        /// Takes a list of reversed digits and returns the value
        ///   ex: 1->2->3 = 321
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static int ReverseDigits(LinkedList<int> list)
        {
            if (list != null)
            {
                if (list.Count > 1)
                {
                    LinkedList<int> copy = new LinkedList<int>();
                    foreach (int val in list)
                        copy.AddLast(val);
                    copy.RemoveFirst();
                    return list.First.Value + 10 * ReverseDigits(copy);
                    //return list.First.Value + 10 * ReverseDigits(list.Next);  // doesn't let us do this? C# is weird
                }
                else
                    return list.First.Value;

            }
            else
                return 0;
        }
    }
}
