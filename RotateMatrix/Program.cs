﻿using System;

namespace RotateMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] val = RotateMatrixQuarter(new int[,]
            {   
                {1,1},
                {2,2}
            });
            val = RotateMatrixQuarter(new int[,]
            {
                {1,1,1},
                {2,2,2},
                {3,3,3}
            });
        }

        /// <summary>
        /// Rotates the matrix clockwise by 90 degress
        ///   Note: assumes square matrix
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        static int[,] RotateMatrixQuarter(int[,] matrix, bool clockwise = true)
        {
            int length = matrix.GetLength(0);
            int[,] rotated = new int[length, length];

            if(clockwise)
            {
                // x1,x2,x3    z1,y1,x1
                // y1,y2,y3 => z2,y2,x2
                // z1,z2,z3    z3,y3,x3
                for (int i = 0; i < length; i++)
                {
                    for (int j = 0; j < length; j++) // needs to be square to rotate
                    {
                        rotated[j, (length - 1) - i] = matrix[i, j];
                        // we're looping through the input matrix and 
                        // placing the elements where they belong in the output matrix
                    }
                }
            }
            else
            {
                // x1,x2,x3    x3,y3,z3
                // y1,y2,y3 => x2,y2,z2
                // z1,z2,z3    x1,y1,z1
                for (int i = 0; i < length; i++)
                {
                    for (int j = 0; j < length; j++) // needs to be square to rotate
                    {
                        rotated[(length - 1) - j, i] = matrix[i, j];
                        // we're looping through the input matrix and 
                        // placing the elements where they belong in the output matrix
                    }
                }
            }

            return rotated;
        }
    }
}
